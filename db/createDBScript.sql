-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Gazdă: 127.0.0.1
-- Timp de generare: dec. 29, 2018 la 10:07 PM
-- Versiune server: 10.1.36-MariaDB
-- Versiune PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Bază de date: `ticalendarac`
--
CREATE DATABASE IF NOT EXISTS `ticalendarac` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `ticalendarac`;

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `rezervari`
--

DROP TABLE IF EXISTS `rezervari`;
CREATE TABLE `rezervari` (
  `idRezervare` int(10) UNSIGNED NOT NULL,
  `tipEveniment` enum('oră de curs','laborator','seminar','proiect','ședința','prezentare firmă','alte activitați cu studenții','alte evenimente') COLLATE utf8_general_ci NOT NULL,
  `coordonatorEveniment` varchar(30) COLLATE utf8_general_ci NOT NULL,
  `numeObiect` varchar(30) COLLATE utf8_general_ci NOT NULL,
  `numeGrupa` varchar(30) COLLATE utf8_general_ci DEFAULT NULL,
  `sala` varchar(30) COLLATE utf8_general_ci NOT NULL,
  `data` date NOT NULL,
  `dataSfarsit` date DEFAULT NULL,
  `oraStart` time NOT NULL,
  `oraSfarsit` time NOT NULL,
  `idUser` int(10) UNSIGNED NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `utilizatori`
--

DROP TABLE IF EXISTS `utilizatori`;
CREATE TABLE `utilizatori` (
  `id` int(10) UNSIGNED NOT NULL,
  `nume` varchar(30) COLLATE utf8_general_ci NOT NULL,
  `prenume` varchar(30) COLLATE utf8_general_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_general_ci NOT NULL,
  `parola` varchar(30) COLLATE utf8_general_ci NOT NULL,
  `tipUser` enum('admin','teacher','guest') COLLATE utf8_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Eliminarea datelor din tabel `utilizatori`
--

INSERT INTO `utilizatori` (`id`, `nume`, `prenume`, `email`, `parola`, `tipUser`) VALUES
(1, 'Andrei', 'Test_admin', 'andrei@mail.com', 'andrei1234', 'admin'),
(2, 'Ana', 'Test_teacher', 'ana@mail.com', 'ana1234', 'teacher'),
(5, 'Maria', 'Test_guest', 'maria@mail.com', 'maria1234', 'guest');

--
-- Indexuri pentru tabele eliminate
--

--
-- Indexuri pentru tabele `rezervari`
--
ALTER TABLE `rezervari`
  ADD PRIMARY KEY (`idRezervare`),
  ADD KEY `idUser` (`idUser`);

--
-- Indexuri pentru tabele `utilizatori`
--
ALTER TABLE `utilizatori`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT pentru tabele eliminate
--

--
-- AUTO_INCREMENT pentru tabele `rezervari`
--
ALTER TABLE `rezervari`
  MODIFY `idRezervare` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pentru tabele `utilizatori`
--
ALTER TABLE `utilizatori`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constrângeri pentru tabele eliminate
--

--
-- Constrângeri pentru tabele `rezervari`
--
ALTER TABLE `rezervari`
  ADD CONSTRAINT `rezervari_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `utilizatori` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

GRANT USAGE ON *.* TO 'adminCalendar'@'localhost' IDENTIFIED BY PASSWORD '*0DF34299F5AC4C03E746FB38198650955BA7EA69';

GRANT ALL PRIVILEGES ON `ticalendarac`.* TO 'adminCalendar'@'localhost';